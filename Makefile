.PHONY: all clean

#-------------------------------------------------------------- user def ---------------------------------------------------------------
# target
TARGET_OBSERVER = Observer
TARGET_PROTOTYPE = Prototype
# build_dir
BUILD_DIR = build
# libs
LIBS =
#---------------------------------------------------------------------------------------------------------------------------------------

all: $(BUILD_DIR)/$(TARGET_OBSERVER) $(BUILD_DIR)/$(TARGET_PROTOTYPE)
$(TARGET_OBSERVER): $(BUILD_DIR)/$(TARGET_OBSERVER)
$(TARGET_PROTOTYPE): $(BUILD_DIR)/$(TARGET_PROTOTYPE)

$(BUILD_DIR):
	mkdir -p $@

CPP_SOURCES_OBSERVER += $(wildcard $(addsuffix /*.cpp,Observer))
CPP_INCLUDES_OBSERVER += $(addprefix -I,./Observer)

CPP_SOURCES_PROTOTYPE += $(wildcard $(addsuffix /*.cpp,Prototype))
CPP_INCLUDES_PROTOTYPE += $(addprefix -I,./Prototype)

# vpath %.cpp $(sort $(dir $(CPP_SOURCES_OBSERVER)))
OBJECTS_OBSERVER = $(addprefix $(BUILD_DIR)/,$(notdir $(CPP_SOURCES_OBSERVER:.cpp=.o)))

# vpath %.cpp $(sort $(dir $(CPP_SOURCES_PROTOTYPE)))
OBJECTS_PROTOTYPE = $(addprefix $(BUILD_DIR)/,$(notdir $(CPP_SOURCES_PROTOTYPE:.cpp=.o)))

$(BUILD_DIR)/%.o: $(TARGET_OBSERVER)/%.cpp | $(BUILD_DIR)
	@echo $(CPP_SOURCES_OBSERVER)
	g++ -c $< -o $@ $(CPP_INCLUDES_OBSERVER) $(LIBS) -MMD -Og -g3

$(BUILD_DIR)/%.o: $(TARGET_PROTOTYPE)/%.cpp | $(BUILD_DIR)
	@echo $(CPP_SOURCES_PROTOTYPE)
	g++ -c $< -o $@ $(CPP_INCLUDES_PROTOTYPE) $(LIBS) -MMD -Og -g3

$(BUILD_DIR)/$(TARGET_OBSERVER): $(OBJECTS_OBSERVER) | $(BUILD_DIR)
	g++ $^ -o $@ $(LIBS) -Og -g3
	@echo $^ 

$(BUILD_DIR)/$(TARGET_PROTOTYPE): $(OBJECTS_PROTOTYPE) | $(BUILD_DIR)
	g++ $^ -o $@ $(LIBS) -Og -g3
	@echo $^

-include $(wildcard $(BUILD_DIR)/*.d)

clean:
	rm -rf $(BUILD_DIR)/*
