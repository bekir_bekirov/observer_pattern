/*
 * main.cpp
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: bekir
 */

#include "main_observer.hpp"
#include "Observer.hpp"


using ptrObserverInterface = std::shared_ptr<ObserverInterface>;

int main(int argc, char **argv)
{
	Publisher publisher;

	PublisherEvent evt1(1, "event1");
	PublisherEvent evt2(2, "event2");

	ptrObserverInterface ptr_obsrv1 = std::make_shared<Observer>(publisher, evt1);
	ptrObserverInterface ptr_obsrv2 = std::make_shared<Observer>(publisher, evt2);
	ptrObserverInterface ptr_obsrv3 = std::make_shared<Observer>(publisher, evt2);

	publisher.Attach(ptr_obsrv1);
	publisher.Attach(ptr_obsrv2);
	publisher.Attach(ptr_obsrv3);

	publisher.Notify(evt1);

	publisher.Detach(ptr_obsrv1);
	publisher.Detach(ptr_obsrv2);

	publisher.Notify(evt2);


//	while(getchar() != 'c') {};
	std::cout << "Program is end!" << std::endl;
}
