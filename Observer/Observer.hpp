/*
 * Observer.hpp
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: bekir
 */

#ifndef OBSERVER_HPP_
#define OBSERVER_HPP_


#include <iostream>
#include <vector>
#include <list>
#include <memory>
#include <algorithm>

class PublisherEventInterface
{
public:
	virtual ~PublisherEventInterface(){};
	virtual void SetEventState(bool state) = 0;
	virtual bool GetEventState(void) const = 0;
	virtual std::string GetEventMsg(void) const = 0;
	virtual size_t GetEventID(void) const = 0;
};

class PublisherEvent : public PublisherEventInterface
{
public:
	~PublisherEvent(){};
	PublisherEvent(size_t evt_id, std::string evt_msg) :
		id(evt_id), flag(0), msg(evt_msg){}

	void SetEventState(bool state) override
	{
		flag = state;
	}

	bool GetEventState(void) const override
	{
		return flag;
	}

	std::string GetEventMsg(void) const override
	{
		return msg;
	}

	size_t GetEventID(void) const override
	{
		return id;
	}

private:
	size_t id;
	bool flag;
	std::string msg;
};

class ObserverInterface
{
public:
	virtual ~ObserverInterface(){}
	virtual void Update(void) = 0;
};

class PublisherInterface
{
public:
	virtual ~PublisherInterface(){}
	virtual void Attach(const std::shared_ptr<ObserverInterface> &observer) = 0;
	virtual void Detach(const std::shared_ptr<ObserverInterface> &observer) = 0;
	virtual void Notify(PublisherEventInterface &evt) = 0;
};

class Publisher : public PublisherInterface
{
public:
	~Publisher(){}

	void Attach(const std::shared_ptr<ObserverInterface> &observer) override
	{
		list_observers.push_back(observer);
	}

	void Detach(const std::shared_ptr<ObserverInterface> &observer) override
	{
		list_observers.remove(observer);
	}

	virtual void Notify(PublisherEventInterface &evt) override
	{
		evt.SetEventState(true);
		for (auto &obsrv : list_observers) {
				obsrv->Update();
		}
		evt.SetEventState(false);
	}

private:
	std::list<std::shared_ptr<ObserverInterface> > list_observers;
};

class Observer : public ObserverInterface
{
public:
	Observer(const PublisherInterface &pub_instance, PublisherEventInterface &evt) :
		publisher(pub_instance), event(evt){}

	~Observer(){}

	void Update(void) override
	{
		if (event.GetEventState()) {
			std::cout << event.GetEventMsg() << std::endl;
		}
	}

private:
	const PublisherInterface &publisher;
	PublisherEventInterface &event;
};

#endif /* OBSERVER_HPP_ */
