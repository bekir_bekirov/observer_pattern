#pragma once

#include <iostream>


class PrototypeInterface
{
protected:
	size_t param;

public:
	PrototypeInterface(size_t param) : param(param){}

	virtual ~PrototypeInterface(){}

	virtual PrototypeInterface *clone(void) = 0;

	virtual size_t GetParam(void)
	{
		return param;
	}
};

class Prototype : public PrototypeInterface
{
private:
//	Prototype *prototype_ptr;
    size_t *data_buf;

public:
	Prototype(size_t param) : PrototypeInterface(param)
	{
		data_buf = new size_t[100];
		std::cout << "Constructor, data_buf has adr: " << data_buf << std::endl; 
	}

	~Prototype()
	{
//		delete prototype_ptr;
        delete[] data_buf;
	}

    Prototype(const Prototype &rhs) : PrototypeInterface(rhs)
    {
    	data_buf = new size_t[100];
    	std::cout << "Copy constructor, data_buf has adr: " << data_buf << std::endl;
    }

    Prototype &operator=(const Prototype &rhs)
    {
        PrototypeInterface::operator=(rhs);

    	if (this == &rhs) {
            return *this;
        }

        delete[] data_buf;
        data_buf = new size_t[100];

        return *this;
    }

	Prototype *clone(void) override
	{
		return new Prototype(*this);
	}
};
