#include "main_prototype.hpp"
#include "Prototype.hpp"

int main(int argc, char **argv)
{
    Prototype prototype(1);

    Prototype prototype_1(2);
    Prototype prototype_2(0);

    PrototypeInterface &ref_prototype = prototype_1;
    prototype_2 = prototype_1; //ref_prototype;

    std::cout << prototype.GetParam() << std::endl;

    PrototypeInterface *prototype_clone = prototype.clone();

	std::cout << "Prototype is finished" << std::endl;

	return 0;
}
